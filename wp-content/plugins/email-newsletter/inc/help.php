<strong>Compose email</strong> 

<p>This is the first step for this plug-in, before sending mail to user, first we have to compose the mail using this page, once you created the mail content from this page, the created mail subject displayed automatically in the send mail page.</p>

<strong>Send mail to registered user</strong> 

<p>This page is to send mail to all registered user, in this page you can find the all registered users email with check box option, if you don't want to send mail to any particular user(s), you can uncheck the email(s) from the list, after you selected the users from the list select the email subject and click send mail button.</p>

<strong>Send mail to comment posted user</strong> 

<p>This page is to send mail to all comment posted user, in this page you can find all the comment posted user(s) email with check box option, if you don't want to send mail to any particular user(s), you can uncheck the email from the list, after you selected the users from the list select the email subject and click send mail button.</p>

<strong>Send mail to subscribed user</strong> 

<p>From version 4.0 we have new option to subscribe the emails. After activated the plugin drag and drop the email newsletter widget to your sidebar, this will create the email subscribe form in the front end. With this form user can subscribe the email(s) to our newsletter.</p>

<p>In the admin we have link "Send mail to subscribed user"; From this menu we can send email to subscribed user(s). In this page admin can see all the subscribed users email id with check-box option. Admin can check/un-check the user and can send the email to subscribed user(s).</p>

<p>In the "View subscribed user" admin menu. we can see all the subscribed user email list.</p>

<strong>Send mail to simple contact form emails</strong> 

<p>This is the new option. In my plugin list you can find the "simple contact form" WordPress plugin. Simple contact form just store the entered details in the admin to view, with the help of this email newsletter plugin you can send the email to those contact list.</p>

<p>Simply saying; i have integrated the email newsletter wordpress plugin & simple contact form wordpress plugin into this page.</p>

<p>Download and set up the simple contact form wordpress plugin to use this feature. If you don't want simple contact form; Simply omit this menu.</p>
<br />