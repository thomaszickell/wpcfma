=== WP Easy Gallery ===
Contributors: hahncgdev
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=PMZ2FPNJPH59U
Tags: gallery, gallery for wordpress, wordpress gallery, easy gallery, images gallery, image gallery, wp gallery, easy image gallery, free photo gallery, wp easy gallery
Requires at least: 2.6
Tested up to: 3.3.1
Stable tag: 1.5

WP Easy Gallery is a gallery plugin for WordPress that allows you to create and manage multiple image galleries through an easy to use admin interface.

== Description ==

WP Easy Gallery is an easy to use WordPress gallery plugin that allows you to create and manage multiple image galleries through a simple admin interface.

<a href='http://labs.hahncreativegroup.com/wordpress-plugins/wp-easy-gallery-pro-simple-wordpress-gallery-plugin/'>Upgrade to WP Easy Gallery Pro</a>

Features include:

- Upload unique thumbnail image and set custom dimensions
- Include multiple galleries on a page/post
- Set the 'sort order' of images in the galleries
- WordPress ShortCodes for easy content integration
- And much more...

For more information about this wordpress gallery plugin: <a href='http://labs.hahncreativegroup.com/wordpress-plugins/easy-gallery/'>WP Easy Gallery</a>

<a href='http://labs.hahncreativegroup.com/wordpress-plugins/wp-easy-gallery-pro-simple-wordpress-gallery-plugin/'>WP Easy Gallery Pro</a>

Pro features include:

- Multiple image uploads to quickly populate your galleries
- Improved admin panel for easier gallery and image navigation
- Galleries use 1st image in gallery for thumbnail by default
- Set global thumbnail dimensions for all galleries - can be overwritten for each gallery
- Pop-up preview for each image - see how each gallery image will look in the pop-up window on your site

<a href='http://labs.hahncreativegroup.com/wordpress-plugins/wp-easy-gallery-pro-simple-wordpress-gallery-plugin/'>Upgrade to WP Easy Gallery Pro</a>

Be sure to also check out these other plugins for WordPress: <a href='http://labs.hahncreativegroup.com/wordpress-plugins/custom-post-donations-pro/'>Custom Post Donations Pro</a>, <a href='http://wordpress.org/extend/plugins/custom-post-donations/'>Custom Post Donations</a>, <a href='http://wordpress.org/extend/plugins/wp-translate/'>WP Translate</a> and <a href='http://wordpress.org/extend/plugins/prosperity/'>Prosperity</a>.

<a href='http://labs.hahncreativegroup.com/wordpress-plugins/'>Plugins for WordPress</a> by HahnCreativeGroup.

== Screenshots ==

1. PrettyPhoto style pop-up for image gallery
2. Gallery Overview admin panel - easily maintain multiple galleries
3. Add Galleries admin panel
4. Edit Galleries admin panel
5. Add Images admin panel

== Installation ==

1. Upload the `EasyGallery` directory to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Create new galleries in the `Add Gallery` Panel
1. Add images to galleries in the `Add Images` Panel
1. Add generated shortcode to content to display gallery

== Frequently Asked Questions ==

**What is the best way to load images?**

* Click the 'Upload Image' button
* Select the 'From Computer' tab
* Click the 'Select Files' button
* Choose the desired image to upload
* After image has completed, click the 'Insert into post' button

**Why can't I add galleries to my site?**

* WP Easy Gallery requires PHP 5 to work properly, please check your web hosting configurations.

== Changelog ==

**Oct. 24, 2011 - v1.0**

* Initial release.

**Nov. 7, 2011 - v1.1**

* Fixed bug that caused screen jumping when gallery launched.

**Nov. 14, 2011 - v1.2**

* Cursor now shows pointer when gallery thumbnail is in hover state.
* Added functionality to show gallery name on hover.

**Dec. 9, 2011 - v1.3**

* Fixed issue preventing custom database table prefixes from passing through

**Jan. 9, 2012 - v1.3.1**

* Fixed issue preventing custom database table prefixes from passing through on gallery specific code

**Feb. 25, 2012 - v1.4**

* Bug fixes addressing undefined variable warnings

**Mar. 3, 2012 - v1.5**

* Additional bug fixes

