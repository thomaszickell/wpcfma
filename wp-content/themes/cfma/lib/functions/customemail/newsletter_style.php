<?php /* twentyten */
$_classes = array(
'newl_body'		=> "    background-color:#dce1e4;
                margin:0px;
                border:none;",
'newl_wrapper'	=> "    width:600px;
                background-color:#000000;
                padding:0px;
                table-layout: auto;
                border:none;",
'newl_wrapper_td'=> "   font-size:13px;
                line-height:18px;
                font-family:Arial;
                text-align: justify;
                vertical-align: top;
                color:#ffffff;
                border:none;",
'rulerspace'	=> "   padding:0px;
                width: 100%;
                height: 20px;
                margin:0px;
                color:#000000;",
'clear'         => "    clear:both;",
'content_line'	=> "    padding-left:20px;
                padding-right:20px;
                padding-bottom:20px;
                width: 560px;
                color:#ffffff;",
'topleftimg_div'    => "   float:left;
                text-align:left;
                width: 186px;
                min-height: 100px;",
'topleftimg_img'    => "margin:0;
                padding:0;
                width: 186px;",
'toprighttext_div'		=> "float:right;
                text-align:right;
                width: 373px;
                min-height: 100px;
                color: #c8b27b;
                font-size: 16px;
                font-weight: normal;",
'p_margin0'		=> "margin: 0;",
'csstitleleft'		=> "margin: 0;
                padding: 0;
                color:#c8b27b;
                font-size:18px;
                font-weight:normal;
                font-style:normal;
                font-family:Arial",
'left'		=> "text-align: left;",
'right'		=> "text-align: right;",
'center'		=> "text-align: center;",
'justify'		=> "text-align: justify;",
'cssruleryellow'		=> "padding:0px;
                width: 350px;
                height: 1px;
                margin-top:0px;
                margin-bottom:20px;
                border-bottom: 1px solid #c8b27b;
                color:#000000;",
'textimgleft_divimg'	=> "float: left;
                margin:0;
                padding:0;
                margin-right:10px;
                width: 150px;
                min-height:90px;",
'textimgleft_divimg_img'	=> "float: left;
                margin:0;
                padding:0;
                width: 150px;
                border:none;",
'textimgleft_divtext'		=> "width: 400px;
                float: left;
                color:#ffffff;",
'textimgright_divtext'		=> "width: 400px;
                float: left;
                margin-right:10px;
                min-height:  90px;
                color:#ffffff;",
'textimgright_divimg'		=> "float: left;
                margin:0;
                padding:0;
                width: 150px;
                min-height: 90px;",
'textimgright_divimg_img'	=> "float: left;
                margin:0;
                padding:0;
                width: 150px;
                border:none;",
'cssimgcenter'		=> "margin:0;
                padding:0;
                border:none;",
'cssimgcenter_div'	=> "margin:0px;
                        padding-bottom:20px;",
                        
                 
);
?>