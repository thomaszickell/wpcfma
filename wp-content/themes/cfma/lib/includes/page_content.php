<!-- Begin content_scroll_pane -->
<div class="content_scroll_pane"> 
	<div class="page-mid-content"> <!-- Begin page-mid-content-->
        <h1><?php the_title(); ?></h1>
        <?php	the_content(); ?>  
	</div> <!-- End page-mid-content-->
</div> 
<!-- End content_scroll_pane-->