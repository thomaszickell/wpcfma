<div style="background-color:#dce1e4;margin:0px;">
<table width="100%" cellspacing="0" cellpadding="0" style="background-color:#dce1e4"><tbody><tr><td align="center" valign="top">              
<table width="600" cellspacing="0" cellpadding="0" style="border:0px solid #afafaf;"><tbody><tr><td>
<table width="600" cellspacing="0" cellpadding="0" style="font-size:14px;color:#ffffff;line-height:150%;font-family:Arial;width:600px;background-color:#000000;padding:0px;text-align: justify;vertical-align: top;table-layout: auto;"><tbody>
     <tr>
        <td colspan="4" style="width:100%;height:20px;margin:0;padding:0px;color:#000000;">&nbsp;-</td>
     </tr>
     <tr class="header">
        <td style="width:20px;margin:0;padding:0px;"></td>
        <td style="text-align:left; width: 186px;min-height: 100px;vertical-align: top;">
            <img style="margin:0;padding:0;width: 186px;" alt="" src="http://cfmart.fr/wp-content/uploads/2012/03/cfma_logo_mini.png"/>
        </td>
        <td style="text-align:right; width: 373px;min-height: 100px;color: #c8b27b;font-size: 16px; font-weight: normal;vertical-align: top;"> 
            <p style="margin: 0;">LUNDI 16 AVRIL 2012</p>     
        </td>
        <td style="width:20px;margin:0;padding:0px;"></td>
     </tr>
     <tr>
        <td colspan="4" style="width:100%;height:20px;margin:0;padding:0px;"></td>
     </tr> 
     <tr>
        <td style="width:20px;margin:0;padding:0px;"></td>
        <td colspan="2" style="width: 560px;padding:5px 0;color:#c8b27b;font-size:18px;font-weight:normal;font-style:normal;font-family:Arial">
            <p style="margin:0px;">EDITO</p>
        </td>
        <td style="width:20px;margin:0;padding:0px;"></td>
     </tr>
     <tr>
        <td style="width:20px;margin:0;padding:0px;"></td>
        <td colspan="2" style="text-align: justify;width: 560px;padding:5px 0;color:#ffffff;">
            <p style="margin:0px;">Sous-traitance / Co-traitance une nuance que la CFMA se propose de porter auprès des grands groupes de Luxes et des TPE et PME de métiers d’art afin que leur relations soient en adéquation avec la volonté des pouvoirs publiques pour le “fait en France “.</p>
        </td>
        <td style="width:20px;margin:0;padding:0px;"></td>
     </tr>     
     <tr>
        <td colspan="4" style="width:100%;height:20px;margin:0;padding:0px;">
            <div style="padding:0px; width: 350px; height: 12px;margin-top:0px;margin-bottom:10px; border-bottom: 1px solid #c8b27b;color:#000000;">&nbsp;-</div>  
        </td>
     </tr>
     <tr>
        <td colspan="4" style="text-align: left;color:#ffffff;">    
        <div style="padding-left:20px;padding-right:20px;width: 560px;">
            <p style="margin: 5px 0; padding: 0;color:#c8b27b;font-size:18px;font-weight:normal;font-style:normal;font-family:Arial">La CFMA change d’image</p>
            <div style="clear: both; height: 1px;">&nbsp;</div>
            <div style="float: left;margin:0;padding:0;margin-right:10px;width: 150px; min-height:  90px;">
                <img border="0" style="float: left;margin:0;padding:0;width: 150px;" alt="" src="http://cfmart.fr/wp-content/uploads/2012/03/cfma_logo_mini.png"/>
            </div>
            <div style="width: 400px; float: left;text-align: justify;">
                <p style="margin: 0; padding: 0;">Face à la conjoncture économique internationale, la CFMA qui a plus d’un demi-siècle d’expérience dans la défense et la promotion des entreprises du secteur des Métiers d’Art se devait de faire “peau neuve”.
Un nouveau logo et un site internet modernisé apportent : une meilleure visibilité, des informations et des réponses claires aux problèmes que rencontrent ses membres qui sont des chefs d’entreprises, répartis dans plus de 15 organisations professionnelles responsables et reconnues.
Au sein de commissions telles que : formation, export, social, fiscalité, elle contribue à l’évolution des métiers de l’Artisant d’Art. La Confédération Française des Métiers d’Art rassemble 40 000 entreprises et près de 250 000 salariés. Acteur incontournable dans l’élaboration des règles qui régissent les différentes activités du secteur, grands nombres d’accords paritaires naissent en son sein. A l’origine du Crédit d’Impôts Métiers d’Art, elle veille à sa pérennisation et travaille à la simplification de sa réglementation.
Principale porte-parole des professionnels auprès des pouvoirs publics, la CFMA participe aux divers événements rythmant la vie des métiers d’Art (Assises, forums, conférences, etc.). Ses nombreux partenaires l’enrichissent de leurs connaissances et rayonnement propres. Elle crée des réseaux dans nombre de domaines. L’ensemble de ses actions permettent à ses adhérents qui se consacrent pleinement à leur métier, d’être tenus informés de toute évolution et d’acquérir ainsi plus de performance.</p>
            </div>
            <div style="clear: both; height: 1px;">&nbsp;</div> 
        </div>
        <div style="padding:0px; width: 350px; height: 12px;margin-top:0px;margin-bottom:10px; border-bottom: 1px solid #c8b27b;">&nbsp;</div>      
        <div style="padding-left:20px;padding-right:20px;width: 560px;">
             <p style="margin: 5px 0; padding: 0;color:#c8b27b;font-size:18px;font-weight:normal;font-style:normal;font-family:Arial">Contrefaçon sur internet : compétence des tribunaux français</p>
            <div style="clear: both; height: 1px;">&nbsp;</div>
            <div style="width: 400px; float: left;margin-right:10px;min-height:  90px;text-align: justify;">
                <p style="margin: 0; padding: 0;">Un créateur, titulaire de modèles et de marques en France, peut-il agir devant les tribunaux français lorsqu’il constate la présence de produits ou de marques contrefaisants sur des sites internet hébergés étrangers?
Les tribunaux français se sont, dans un premier temps, déclarés compétents. Mais la jurisprudence exige aujourd’hui, pour ne pas conférer systématiquement une compétence territoriale aux juridictions françaises, au seul motif que les faits incriminés ont pour support technique le réseau internet auquel peut se connecter tout internaute français, que soit démontré que les produits contrefaisants sont destinés au public français.
Les tribunaux se déclarent donc incompétents pour connaître des demandes en contrefaçon dirigées contre des exploitants de sites internet étrangers, rédigés en langue anglaise par exemple, si ceux-ci n’ont aucun impact sur le public français.
Ainsi, la Cour d’Appel de Paris a jugé très récemment que le Tribunal de Grande Instance de Paris était incompétent pour connaître d’une action en contrefaçon et en concurrence déloyale intentée par des artisans couteliers français qui avaient constaté, sur plusieurs sites internet germanophones, la présence de couteaux contrefaisants.
Et selon la Cour d’Appel : « la livraison d’un couteau en France ne suffit pas à démontrer que ces sites internet sont effectivement destinés au public français et à caractériser l’existence d’un lien suffisant, substantiel et significatif entre les faits allégués et le territoire français. En effet, les circonstances particulières de cette unique livraison, réalisée à la requête d’un client des demandeurs, laisse présumer qu’elle a été suscitée dans la seule perspective de la présente action »(Cour d’Appel de Paris, 13 septembre 2011).
Mais le Tribunal de Grande Instance de Paris est en revanche compétent pour statuer sur des demandes en contrefaçon à l’encontre de personnes exploitant des sites internet hébergés dans l’un des 27 pays membre de l’Union Européenne, si le créateur a pris le soin de procéder à des dépôts de modèles (ou de marques) communautaires, lesquels lui confèreront une protection sur l’ensemble de ce territoire.
Face à la mondialisation et à la prolifération des contrefaçons sur internet, le dépôt communautaire présente donc un intérêt certain. Il permet, au moins sur le territoire européen, de mieux protéger ses droits de propriété intellectuelle lorsque ceux-ci sont contrefaits sur internet.</p>
            </div>
            <div style="float: left;margin:0;padding:0;width: 150px; min-height:  90px;">
                <img border="0" style="float: left;margin:0;padding:0;width: 150px;" alt="" src="http://cfmart.fr/wp-content/uploads/2012/03/justice-logo-pro_00FA000000313681.jpg"/> 
            </div>
            <div style="clear: both; height: 1px;">&nbsp;</div> 
        </div>
        <div style="padding:0px; width: 350px; height: 12px;margin-top:0px;margin-bottom:10px; border-bottom: 1px solid #c8b27b;">&nbsp;</div>
        <div style="padding-left:20px;padding-right:20px;width: 560px;">
            <p style="margin: 5px 0; padding: 0;color:#c8b27b;font-size:16px;line-height:20px;font-weight:normal;font-style:normal;font-family:Arial">Système de référencement sur internet et droit des marques.</p>
            <p style="margin: 0; padding: 0;text-align: justify;">Il est désormais possible pour une entreprise, sous certaines conditions, d’utiliser dans le cadre de systèmes de référencement publicitaire sur internet, tel le système « AdWords » de Google, la marque d’autrui à titre de mot-clé.
Selon la jurisprudence en effet, l’achat de mots-clés, constitués exclusivement ou partiellement d’une marque, n’est plus en soi contrefaisant.
Tout dépend en réalité de la façon dont l’annonce sera présentée.
La contrefaçon sera retenue lorsque la publicité suggèrera l’existence d’un lien économique entre l’annonceur et le titulaire de la marque. Cela sera le cas si, par exemple, l’annonceur fait figurer dans sa publicité un signe reproduisant ou imitant la marque.
Au contraire, la contrefaçon sera écartée si le message commercial identifie clairement l’annonceur et si son contenu exclut tout rattachement possible avec le titulaire de la marque.
Cette jurisprudence, qui a pour conséquence d’autoriser n’importe qui à exploiter la marque d’autrui pour les besoins de sa publicité sur internet, est très certainement critiquable puisqu’elle va à l’encontre des intérêts des entreprises.
Les titulaires de marques doivent au surplus désormais faire preuve de prudence en la matière.
Très récemment en effet, la Cour d’Appel de Lyon est allée jusqu’à condamner pour concurrence déloyale le titulaire d’une marque qui était intervenu auprès d’un prestataire internet afin de faire supprimer le mot-clé (constitué de sa propre marque) qui avait été choisi par un annonceur dans le cadre de services de référencement sur internet (Cour d’Appel de Lyon 19 janvier 2012) !</p>
<p>Pierre GREFFE Avocat à la Cour - <a target="_blank" style="color:#fffff !important;text-decoration: none;" href="http://www.cabinet-greffe.com">www.cabinet-greffe.com</a></p>
            <div style="padding-top: 10px;">
                <a style="color:#c8b27b;text-decoration: none;" href="http://cfmart.fr/">http://cfmart.fr/</a>
            </div>
        </div>
<div style="color: #c8b27b; font-size: 14px; font-family: Arial; font-weight: normal; text-align: letf; width: 560px; padding: 20px; min-height: 50px;">
</div> 
</td></tr></tbody></table>      
</td></tr></tbody></table>         
</td></tr></tbody></table>         
</div>